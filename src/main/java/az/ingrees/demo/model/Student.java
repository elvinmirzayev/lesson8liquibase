package az.ingrees.demo.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Student {

    @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    String lastname;
    Double age;
    String phone;
    String middleName;
}
