package az.ingrees.demo.service;

import az.ingrees.demo.model.Student;

public interface StudentService {
    Student get(Integer id);

    Student create(Student student);

    Student update(Long id, Student student);

    Student update(Integer id, Student student);

    void delete(Integer id);
}
